<?php

use Illuminate\Database\Seeder;

class PermissionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name'=>'users'
            ],
            [
                'name'=>'admins'
            ],
            [
                'name'=>'albums'
            ]
        ]);
        DB::table('rules')->insert([
            [
                'name'=>'read',
                'permission_id'=>DB::table('permissions')->where('name','users')
                ->first()->id
            ],[
                'name'=>'create',
                'permission_id'=>DB::table('permissions')->where('name','users')
                ->first()->id
            ],[
                'name'=>'update',
                'permission_id'=>DB::table('permissions')->where('name','users')
                ->first()->id
            ],[
                'name'=>'delete',
                'permission_id'=>DB::table('permissions')->where('name','users')
                ->first()->id
            ],
            [
                'name'=>'read',
                'permission_id'=>DB::table('permissions')->where('name','admins')
                ->first()->id
            ],[
                'name'=>'create',
                'permission_id'=>DB::table('permissions')->where('name','admins')
                ->first()->id
            ],[
                'name'=>'update',
                'permission_id'=>DB::table('permissions')->where('name','admins')
                ->first()->id
            ],[
                'name'=>'delete',
                'permission_id'=>DB::table('permissions')->where('name','admins')
                ->first()->id
            ],[
                'name'=>'read',
                'permission_id'=>DB::table('permissions')->where('name','albums')
                ->first()->id
            ],[
                'name'=>'delete',
                'permission_id'=>DB::table('permissions')->where('name','albums')
                ->first()->id
            ],
        ]);
        $allRules = DB::table('rules')->pluck('id');
        $user = DB::table('admins')->insertGetId(['email'=>'admin@admin.com','password'=>bcrypt('123456')]);
        foreach ($allRules as $rule){

            DB::table('admin_rules')->insert(['admin_id'=>$user,'rule_id'=>$rule]);
        }
    }
}
