<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::group(['as'=>'site.','namespace'=>'Site'],function (){
    Route::get('public-albums','AlbumController@index');
    Route::get('profile','ProfileController@show');
    Route::get('profile-albums','ProfileController@privateAlbums');
    Route::post('add-album','ProfileController@addAlbum');
    Route::post('edit-album/{id}','ProfileController@editAlbum');
    Route::get('delete-album','ProfileController@delAlbum');
});

Route::group(['as'=>'dashboard.','prefix'=>'dashboard','namespace'=>'Admin'], function () {
    Route::get('logout',function (){
        admin()->logout();
        return redirect()->route('dashboard.login');
    });
    Route::view('login','admin.login')->middleware('guest');
    Route::post('login','HomeController@login')->name('login');
    Route::group(['middleware'=>'Admin'],function (){
        Route::get('/','HomeController@index')->name('main');
        Route::resource('admins','AdminController');
        Route::resource('users','UserController');
        Route::resource('albums','AlbumController');
    });
});
Route::view('/', 'website.home');
Route::view('profile', 'website.profile');
