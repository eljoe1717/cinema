<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;
    protected $guarded = ['id'];
    public $timestamps = ['created_at','updated_at'];

    public function albums()
    {
        return $this->hasMany(Album::class,'user_id');
    }

    public function privateAlbums()
    {
        return $this->hasMany(Album::class,'user_id')->where('type','private');
    }
}
