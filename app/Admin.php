<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    protected $fillable = ['password','email'];
    public $timestamps = ['created_at','updated_at'];

    public function _can($string)
    {
        $explode = explode(' ',$string);
        return $this->permissions()->where('rule_name',$explode[0])->where('permission_name',$explode[1])->count() != 0;
    }

    public function rules()
    {
        return $this->belongsToMany(Rule::class,'admin_rules','admin_id','rule_id');
    }

    public function permissions()
    {
        $permissions = $this->rules()->get()->transform(function ($rule){
            return [
                'rule_id'=> $rule->id,
                'rule_name'=>$rule->name,
                'permission_name'=>$rule->permission->name
            ];
        });
        return collect($permissions);
    }
}
