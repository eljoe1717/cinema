<?php

namespace App;

use App\Http\Traits\ImageOps;
use App\Http\Traits\LangOps;
use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    use ImageOps,LangOps;
    protected $guarded = ['id'];
    public $timestamps = ['created_at','updated_at'];

    public function photos()
    {
        return $this->hasMany(AlbumImage::class,'album_id');
    }

    public function getPriceAfterDiscountAttribute()
    {
        return $this->price - $this->price*$this->discount_percentage/100;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
