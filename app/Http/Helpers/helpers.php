<?php

/**
 * Upload Path
 * @return string
 */
function uploadpath()
{
    return 'photos';
}

/**
 * Get Image
 * @param $filename
 * @return string
 */
function getimg($filename)
{
    if (!empty($filename)) {
        $base_url = url('/');
        return $base_url . '/storage/' . $filename;
    } else {
        return null;
    }
}

/**
* Upload an image
* @param $img
*/

function uploader($request, $img_name)
{
    $path = \Storage::disk('public')->putFile(uploadpath(), $request->file($img_name));
    return $path;
}
/**
 * @param $request
 * @param $img_name
 * @param $model
 * @param array $options
 * @return mixed
 */
function multipleUploader($request, $img_name, $model, $options = [])
{
    foreach ($request->$img_name as $key => $item) {
        $photo = \Illuminate\Support\Facades\Storage::disk('public')->putFile(uploadpath(), $item);
        $items[$key] = $model->firstOrCreate([
            $img_name => $photo
        ], $options);
    }

    return $items;
}

/**
 * @param $collection
 * @param $target
 * @return mixed
 */
function getLang($collection, $target)
{
    if (app()->getLocale() == 'en') {
        return $collection['en_' . $target];
    } else {
        return $collection['ar_' . $target];
    }
}

function admin(){
    return auth('admin');
}

function popup($name)
{
    $ms = 5000;
    $types = ['success', 'warning', 'info', 'error', 'basic'];
    $crud = ['add', 'update', 'delete'];

    if (in_array($name, $crud)) {
        if ($name == 'add') {
            return alert()->success('تم الاضافه بنجاح')->autoclose($ms);
        } elseif ($name == 'update') {
            return alert()->success('تم التعديل بنجاح')->autoclose($ms);
        } elseif ($name == 'delete') {
            return alert()->success('تم الحذف بنجاح')->autoclose($ms);
        }
    }

    if (in_array(array_keys($name)[0], $types)) {
        $alert = array_keys($name)[0];
        return alert()->$alert(array_values($name)[0])->autoclose($ms);
    }


    if (array_keys($name)[0] == 'rules') {

        $validator = \Illuminate\Support\Facades\Validator::make(request()->all(), array_values($name)[0]);

        if ($validator->fails()) {
            alert()->error($validator->errors()->first())->autoclose($ms);
            return true;
        }
        return false;

    }


}

function myPerm($perm,$name){
    return collect($perm)->where('permission_name',$name)->pluck('rule_id')->toArray();
}

function canDo($perm,$string){
    $exp = explode(' ',$string);
    return collect($perm)->where('rule_name',$exp[0])->where('permission_name',$exp[1])->count() != 0;
}
