<?php

namespace App\Http\Resources\Site;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AlbumResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'albums'=> $this->collection->transform(function ($q){
                return [
                    'id'=>$q->id,
                    'name'=>$q->name,
                    'ar_name'=>$q->ar_name,
                    'en_name'=>$q->en_name,
                    'image'=>$q->image,
                    'price'=>$q->price,
                    'type'=>$q->type,
                    'price_after_discount'=>number_format($q->price_after_discount,2),
                    'rate'=> rand(0,5),
                    'discount_percentage'=> $q->discount_percentage
                ];
            }),
            'paginate'=>[
                'total' => $this->total(),
                'count' => $this->count(),
                'per_page' => $this->perPage(),
                'next_page_url'=>$this->nextPageUrl(),
                'prev_page_url'=>$this->previousPageUrl(),
                'current_page' => $this->currentPage(),
                'total_pages' => $this->lastPage()
            ]
        ];
    }
}
