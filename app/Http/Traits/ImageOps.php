<?php
namespace App\Http\Traits;
trait ImageOps {

    public function getImageAttribute($value)
    {
        return getimg($value);
    }

    public function setImageAttribute($value)
    {
        $this->attributes['image'] = \Storage::disk('public')->putFile('photos', $value);
    }

}
