<?php
namespace App\Http\Traits;

trait LangOps {
    public function getNameAttribute()
    {
        return getLang($this,'name');
    }
}
