<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (admin()->check()){
            $permissions = admin()->user()->permissions();
            \View::share('permissions',$permissions);
            return $next($request);
        }
        popup([
            'warning'=>'You dont have permissions to pass here !'
        ]);
        return redirect('/');
    }
}
