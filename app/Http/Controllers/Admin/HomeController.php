<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Album;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $data['albums'] = Album::count();
        $data['admins'] = Admin::count();
        $data['users'] = User::count();
        return view('admin.home',compact('data'));
    }

    public function login(Request $request)
    {
        $request->validate([
            'email'=>'required|email|exists:admins,email',
            'password'=>'required'
        ]);
        if (admin()->attempt($request->only(['email','password']))){
            return redirect()->intended('/dashboard');
        }
        return back()->with('errors',collect(['Invalid Credentials']));

    }
}
