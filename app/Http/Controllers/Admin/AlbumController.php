<?php

namespace App\Http\Controllers\Admin;

use App\Album;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AlbumController extends Controller
{
    public function index()
    {
        if (!\admin()->user()->_can('read albums')){
            popup(['warning'=>'no permission to pass here']);
            return back();
        }

        $albums = Album::with('photos')->paginate(10);
        return view('admin.albums.index',compact('albums'));
    }

    public function destroy($id)
    {
        if (!\admin()->user()->_can('delete admins')){
            popup(['warning'=>'no permission to pass here']);
            return back();
        }

        Album::find($id)->delete();
        popup('delete');
        return back();
    }
}
