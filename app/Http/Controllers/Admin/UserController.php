<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!\admin()->user()->_can('read users')){
            popup(['warning'=>'no permission to pass here']);
            return back();
        }

        $users = User::paginate(10);
        return view('admin.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!\admin()->user()->_can('create users')){
            popup(['warning'=>'no permission to pass here']);
            return back();
        }

        return  view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = popup(['rules'=>[
            'name'=>'required',
            'email'=>'required|email|unique:users,email',
            'password'=>'required|confirmed|min:6'
        ]]);
        if ($validate)
            return back()->withInput($request->all());
        $inputs = $request->all();
        $inputs['password'] = bcrypt($request->password);
        User::create($inputs);
        popup('add');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!\admin()->user()->_can('delete users')){
            popup(['warning'=>'no permission to pass here']);
            return back();
        }

        if ($id != \admin()->id()){
            User::find($id)->delete();
            popup('delete');
        }else{
            popup(['warning'=>'Cant Delete Your Account']);
        }
        return back();

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!\admin()->user()->_can('update users')){
            popup(['warning'=>'no permission to pass here']);
            return back();
        }
        $user = User::find($id);
        return view('admin.users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = popup(['rules'=>[
            'name'=>'required',
            'email'=>'required|email|unique:users,email',
            'password'=>'nullable|min:6|confirmed'
        ]]);
        if ($validate)
            return back()->withInput($request->all());
        $inputs = $request->except('password','password_confirmation');
        if ($request->has('password') && $request->password != null)
            $inputs['password'] = bcrypt($request->password);

        User::find($id)->update($inputs);
        popup('update');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
