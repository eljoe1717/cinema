<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!\admin()->user()->_can('read admins')){
            popup(['warning'=>'no permission to pass here']);
            return back();
        }
        $admins = Admin::paginate(10);
        return view('admin.admins.index',compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!\admin()->user()->_can('create admins')){
            popup(['warning'=>'no permission to pass here']);
            return back();
        }
        return  view('admin.admins.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = popup(['rules'=>[
            'email'=>'required|email|unique:admins,email',
            'password'=>'required|confirmed|min:6'
        ]]);
        if ($validate)
            return back()->withInput($request->all());
        $inputs = $request->all();
        $inputs['password'] = bcrypt($request->password);
        $admin = Admin::create($inputs);
        $admin->rules()->sync($request->permission);
        popup('add');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!\admin()->user()->_can('delete admins')){
            popup(['warning'=>'no permission to pass here']);
            return back();
        }
        if ($id != \admin()->id()){
            Admin::find($id)->delete();
            popup('delete');
        }else{
            popup(['warning'=>'Cant Delete Your Account']);
        }
        return back();

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!\admin()->user()->_can('update admins')){
            popup(['warning'=>'no permission to pass here']);
            return back();
        }
        $admin = Admin::find($id);
        return view('admin.admins.edit',compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = popup(['rules'=>[
            'email'=>'required|email|unique:admins,email,'.$id,
            'password'=>'nullable|min:6|confirmed',
            'permission.*'=>'required'
        ]]);
        if ($validate)
            return back()->withInput($request->all());
        $inputs = $request->except('password','password_confirmation');
        if ($request->has('password') && $request->password != null)
            $inputs['password'] = bcrypt($request->password);

        $admin = Admin::find($id);
        $admin->update($inputs);

        $admin->rules()->sync($request->permission);
        popup('update');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
