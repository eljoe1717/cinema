<?php

namespace App\Http\Controllers\Site;

use App\Album;
use App\Http\Resources\Site\AlbumResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AlbumController extends Controller
{
    public function index()
    {
        $albums = Album::whereType('public')->paginate(6);
        return response()->json(new AlbumResource($albums));
    }
}
