<?php

namespace App\Http\Controllers\Site;

use App\Album;
use App\Http\Resources\Site\AlbumResource;
use App\Http\Traits\ApiResponses;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class ProfileController extends Controller
{
    use ApiResponses;
    public function show()
    {
        $user = auth()->user();
        return view('website.profile',compact('user'));
    }

    public function privateAlbums()
    {
        $albums = auth()->user()->privateAlbums()->paginate(6);
        return response()->json(new AlbumResource($albums));
    }

    public function addAlbum(Request $request)
    {
        $rules = [
            'ar_name' => 'required',
            'en_name' => 'required',
            'discount_percentage'=>'required',
            'price'=>'required',
            'type'=>'required',
            'image'=>'required|image'
        ];

        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}

        $q = auth()->user()->albums()->create($request->all());
        if ($request->type == 'private')
            $data = [
                'id'=>$q->id,
                'name'=>$q->name,
                'ar_name'=>$q->ar_name,
                'en_name'=>$q->en_name,
                'image'=>$q->image,
                'price'=>$q->price,
                'type'=>$q->type,
                'price_after_discount'=>number_format($q->price_after_discount,2),
                'rate'=> rand(0,5),
                'discount_percentage'=> $q->discount_percentage
            ];
        else $data = [];

        return $this->apiResponse($data);
    }
    public function editAlbum(Request $request,$id)
    {
        $rules = [
            'ar_name' => 'required',
            'en_name' => 'required',
            'discount_percentage'=>'required',
            'price'=>'required',
            'image'=>'nullable|image'
        ];

        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}

        $inputs = $request->except('image');
        if ($request->image != null){
            $inputs['image'] = $request->image;
        }

        $q = auth()->user()->albums()->find($id);
        $q->update($inputs);
            $data = [
                'id'=>$q->id,
                'name'=>$q->name,
                'ar_name'=>$q->ar_name,
                'en_name'=>$q->en_name,
                'image'=>$q->image,
                'price'=>$q->price,
                'type'=>$q->type,
                'price_after_discount'=>number_format($q->price_after_discount,2),
                'rate'=> rand(0,5),
                'discount_percentage'=> $q->discount_percentage
            ];

        return $this->apiResponse($data);
    }

    public function delAlbum(Request $request)
    {
        Album::find($request->id)->delete();
        return response()->json(['status'=>true]);
    }
}
