<?php

namespace App;

use App\Http\Traits\ImageOps;
use App\Http\Traits\LangOps;
use Illuminate\Database\Eloquent\Model;

class AlbumImage extends Model
{
    use ImageOps , LangOps;
    protected $guarded = ['id'];
    public $timestamps = ['created_at','updated_at'];
}
