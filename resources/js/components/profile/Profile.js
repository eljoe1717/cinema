import React, {Component} from 'react';
import ReactDOM from "react-dom";
import Album from "./Album";
import {isEmpty} from "../../helpers/Helpers";
import AddModal from "./AddModal";
import EditModal from "./EditModal";
import withReactContent from 'sweetalert2-react-content'
import Swal from 'sweetalert2';
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            albums : [],
            selectedAlbum : {},
            paginate : []
        };

        this.sendData = this.sendData.bind(this);
        this.deleteAlbum = this.deleteAlbum.bind(this);
        this.openEditModal = this.openEditModal.bind(this);
        this.sendEditData = this.sendEditData.bind(this);
        this.getMoreResults = this.getMoreResults.bind(this);
    }

    async componentDidMount() {
        var albums = await (await fetch(`/profile-albums`)).json();
        this.setState({...albums});
    }

    async getMoreResults(){
        var albums = await (await fetch(`${this.state.paginate.next_page_url}`)).json();
        this.setState({
            albums : [...albums.albums,...this.state.albums],
            paginate : albums.paginate
        });
    }
    openAddModal(){
        $('#addModal').modal('show');
    }
    openEditModal(album){
        $('#editModal').modal('show');
        this.setState({
            selectedAlbum : album
        })
    }

    async sendData(){
        var form = $('.addAlbum')[0];
        var data = new FormData(form);
        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': token
            }
        });
        var data = await $.ajax({
            url : "/add-album",
            type : 'post',
            data : data,
            enctype: 'multipart/form-data',
            processData: false,  // Important!
            contentType: false,
            cache: false,
        });

        if (data.value == true){
            if (data != []){
                var newData = this.state.albums.concat(data.data);
                this.setState({
                    albums : newData
                });
            }
            form.reset();
            $('#addModal').modal('hide');
            Toast.fire({
                title: 'Album added successful .',
                type: 'success'
            });
        }else{
            Toast.fire({
                title: data.msg,
                type: 'error'
            });
        }
    }
    async sendEditData(albumId){
        var form = $('.editAlbum')[0];
        var data = new FormData(form);
        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': token
            }
        });
        var data = await $.ajax({
            url : "/edit-album/"+albumId,
            type : 'post',
            data : data,
            enctype: 'multipart/form-data',
            processData: false,  // Important!
            contentType: false,
            cache: false,
        });

        if (data.value == true){
            var newData = data.data;
            var albums = this.state.albums.map(album=>{
                if(newData.id == album.id){
                    return newData;
                }else{
                    return album;
                }
            });
            this.setState({
                albums : albums
            });
            $('#editModal').modal('hide');
            Toast.fire({
                title: 'Album updated successful .',
                type: 'success'
            });
        }else{
            Toast.fire({
                title: data.msg,
                type: 'error'
            });
        }
    }

    async deleteAlbum(id){
        axios('delete-album',{params:{id}});
        var albums = this.state.albums.filter(alb=>{
            return alb.id != id;
        });
        this.setState({
            albums
        });

        Toast.fire({
            title: 'Album deleted successful .',
            type: 'error'
        });
    }




    render() {
        let {albums,paginate} = this.state;
        return (
            <section className="check_demo_movie">
                <div className="container">
                    <div className="row">
                        <button className="col-sm-4 btn btn-info" onClick={this.openAddModal}>Add</button>
                    </div>
                    <div className="row">
                        {!isEmpty(albums) &&
                        albums.map((album,index)=><Album removeAlbum={this.deleteAlbum} editAlbum={()=>this.openEditModal(album)} key={index} album={album} />)
                        }
                    </div>
                    <AddModal sendData={this.sendData} />
                    <EditModal album={this.state.selectedAlbum} sendEditData={this.sendEditData} />
                    <hr/>
                    {
                        paginate.next_page_url != null &&
                        <button className="btn btn-info text-center" onClick={this.getMoreResults}>more ....</button>
                    }
                </div>
            </section>
        );
    }
}

if (document.getElementById('profile')) {
    ReactDOM.render(<Profile />, document.getElementById('profile'))
}
