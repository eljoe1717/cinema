import React, {Component} from 'react';

class AddModal extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="modal fade" id="addModal" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Add Album</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form className="addAlbum">
                                <div className="content">
                                    <div className="form-group">
                                        <label>ar Name :</label>
                                        <input type="text" name="ar_name" className="form-control" required/>
                                    </div>
                                    <div className="form-group">
                                        <label>en Name :</label>
                                        <input type="text" name="en_name" className="form-control" required/>
                                    </div>
                                    <div className="form-group">
                                        <label>price :</label>
                                        <input type="number" name="price" className="form-control" required/>
                                    </div>
                                    <div className="form-group">
                                        <label>discount percentage :</label>
                                        <input type="number" name="discount_percentage" className="form-control" required/>
                                    </div>
                                    <div className="form-group">
                                        <label>Type :</label>
                                        <select name="type" className="form-control" required>
                                            <option value="public">public</option>
                                            <option value="private">private</option>
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label>Image :</label>
                                        <input type="file" name="image" className="form-control" required/>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close
                            </button>
                            <button type="button" className="btn btn-primary" onClick={this.props.sendData}>Save</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AddModal;
