import React, {Component} from 'react';
import AlbumRate from "../app/AlbumRate";

class Album extends Component {
    constructor(props) {
        super(props);
        this.state = {...props};
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props != prevProps){
            this.setState({...this.props});
        }
    }

    render() {
        let {album} = this.state;
        return (
                <div className="col-md-4">
                    <div className="card wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                        <div className="card-header">
                            <img src={album.image} className="lazyload" />
                        </div>
                        <div className="card-body">
                            <h4><a href="#"> {album.name}</a></h4>
                            <AlbumRate rate={album.rate} />
                            <p className="package-price">
                                <span>{album.price}$ </span>
                                {album.price != album.price_after_discount &&
                                <span><del className="text-danger">{album.price_after_discount}$</del></span>
                                }
                            </p>
                        </div>
                        <div className="container">
                            <button className="col-sm-6 btn btn-info" onClick={()=> this.props.editAlbum(album)}>edit</button>
                            <button className="col-sm-6 btn btn-danger" onClick={()=>this.props.removeAlbum(album.id)}>delete</button>
                        </div>
                    </div>
                </div>
        );
    }
}

export default Album;
