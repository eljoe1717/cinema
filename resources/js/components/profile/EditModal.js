import React, {Component} from 'react';

class EditModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            album: {}
        };
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevProps.album != this.props.album){
            this.setState({
                album : this.props.album
            })
        }
    }

    render() {
        var {album} = this.state;
        return (
            <div className="modal fade" id="editModal" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Edit Album</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form className="editAlbum">
                                <div className="content">
                                    <div className="form-group">
                                        <label>ar Name :</label>
                                        <input type="text" name="ar_name" defaultValue={album.ar_name} className="form-control" required/>
                                    </div>
                                    <div className="form-group">
                                        <label>en Name :</label>
                                        <input type="text" name="en_name" defaultValue={album.en_name} className="form-control" required/>
                                    </div>
                                    <div className="form-group">
                                        <label>price :</label>
                                        <input type="number" name="price" defaultValue={album.price} className="form-control" required/>
                                    </div>
                                    <div className="form-group">
                                        <label>discount percentage :</label>
                                        <input type="number" defaultValue={album.discount_percentage} name="discount_percentage" className="form-control" required/>
                                    </div>
                                    <div className="row text-center justify-content-center">
                                        <img src={album.image} alt="ops" height="200" width="200"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Image :</label>
                                        <input type="file" name="image" className="form-control" required/>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close
                            </button>
                            <button type="button" className="btn btn-primary" onClick={()=>this.props.sendEditData(album.id)}>Save</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default EditModal;
