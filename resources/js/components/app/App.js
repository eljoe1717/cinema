import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Album  from "./Album";
import {isEmpty} from "../../helpers/Helpers";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            albums : [],
            paginate : []
        };
        this.getMoreResults = this.getMoreResults.bind(this);
    }


    async componentDidMount() {
        var albums = await (await fetch(`/public-albums`)).json();
        this.setState({...albums});
    }

    async getMoreResults(){
        var albums = await (await fetch(`${this.state.paginate.next_page_url}`)).json();
        this.setState({
            albums : [...albums.albums,...this.state.albums],
            paginate : albums.paginate
        });
    }


    render() {
        return (
            <section className="check_demo_movie">
                <div className="container">
                    <h2 className=" wow fadeInDown">Check Our <span className="main-color"> Packages</span></h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the
                        industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of
                        type
                        and scrambled it to make a type specimen book.</p>
                    <div className="row">
                        {!isEmpty(this.state.albums) &&
                        this.state.albums.map((album,index)=> <Album key={index} album={album} />)
                        }
                    </div>
                </div>
                <hr/>
                {
                    this.state.paginate.next_page_url != null &&
                    <button className="btn btn-info text-center" onClick={this.getMoreResults}>more ....</button>
                }
            </section>
    );
    }
}

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'))
}
