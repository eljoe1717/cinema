import React, {Component} from 'react';
import AlbumRate from "./AlbumRate";
class Album extends Component {
    constructor(props){
        super(props);
        this.state = {
            ...props
        }
    }
    render() {
        let {album} = this.state;
        return (
                <div className="col-md-4">
                    <div className="card wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                        <div className="card-header">
                            <img src={album.image} className="lazyload" />
                        </div>
                        <div className="card-body">
                            <h4><a href="#"> {album.name}</a></h4>
                            <AlbumRate rate={album.rate} />
                            <p className="package-price">
                                <span>{album.price}$ </span>
                                {album.price != album.price_after_discount &&
                                <span><del className="text-danger">{album.price_after_discount}$</del></span>
                                }
                            </p>
                        </div>
                    </div>
                </div>
        );
    }
}

export default Album;
