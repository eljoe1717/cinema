import React, {Component} from 'react';

class AlbumRate extends Component {
    constructor(props) {
        super(props);
        this.state = {...props};
    }

    render() {
        return (
            <div className="rating">
                <ul className="d-flex justify-content-center rating_stars">
                    <li><i className="fas fa-star star_gold"></i></li>
                    <li><i className="fas fa-star star_gold"></i></li>
                    <li><i className="fas fa-star star_gold"></i></li>
                    <li><i className="fas fa-star star_gold"></i></li>
                    <li><i className="fas fa-star star_gold"></i></li>
                </ul>
            </div>
        );
    }
}

export default AlbumRate;
