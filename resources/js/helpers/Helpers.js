export function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

export function idInArray(needle,array) {
    var newArr = [];
    array.forEach(function (value) {
        if (value.id==needle){
            newArr.push(needle);
        }
    });
    if (newArr.length != 0)
        return  true;
    return false;
}

export function pluck(arr,key) {
    var newArr = [];
    $.map(arr, function(e) { return newArr.push(e[key]); })
    return newArr;
}
