<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="{{asset('admin/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('admin/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('admin/assets/css/core.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('admin/assets/css/components.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('admin/assets/css/colors.min.css')}}" rel="stylesheet" type="text/css">
<!-- /global stylesheets -->

<!-- Core JS files -->
<script type="text/javascript" src="{{asset('admin/assets/js/plugins/loaders/pace.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/assets/js/core/libraries/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/assets/js/core/libraries/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/assets/js/plugins/loaders/blockui.min.js')}}"></script>
<!-- /core JS files -->
<script type="text/javascript" src="{{asset('admin/assets/js/plugins/forms/selects/bootstrap_select.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/assets/js/pages/form_bootstrap_select.js')}}"></script>

<!-- Theme JS files -->
<script type="text/javascript" src="{{asset('admin/assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/assets/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/assets/js/plugins/ui/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/assets/js/plugins/pickers/daterangepicker.js')}}"></script>

<script type="text/javascript" src="{{asset('admin/assets/js/core/app.min.js')}}"></script>
{{--<script type="text/javascript" src="{{asset('admin/assets/js/pages/dashboard.js')}}"></script>--}}

<!-- /theme JS files -->
<style>
    .icons-list li {
        float: left;
    }

    .icons-list li a {
        display: block;
        padding: 5px;
    }
</style>
