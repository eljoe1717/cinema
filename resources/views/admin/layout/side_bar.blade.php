<!-- Main sidebar -->
<div class="sidebar sidebar-main">
    <div class="sidebar-content">


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <!-- Main -->
                    <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                    <li class="{{request()->is('*dashboard') ? 'active' : ''}}"><a href="{{route('dashboard.main')}}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                    @if(canDo($permissions,'read users'))
                    <li>
                        <a href="#" class="{{request()->is('*users*') ? 'active' : ''}}"><i class="icon-stack2"></i> <span>Users</span></a>
                        <ul>
                            <li class="{{request()->is('*users*') ? 'active' : ''}}"><a href="{{route('dashboard.users.index')}}">All Users</a></li>
                            @if(canDo($permissions,'create users'))
                            <li class="{{request()->is('*users/create') ? 'active' : ''}}"><a href="{{route('dashboard.users.create')}}">Add User</a></li>
                                @endif
                        </ul>
                    </li>
                    @endif
                    @if(canDo($permissions,'read admins'))
                    <li>
                        <a href="#" class="{{request()->is('*admins*') ? 'active' : ''}}"><i class="icon-stack2"></i> <span>Admins</span></a>
                        <ul>
                            <li class="{{request()->is('*admins*') ? 'active' : ''}}"><a href="{{route('dashboard.admins.index')}}">All Admins</a></li>
                            @if(canDo($permissions,'create users'))
                            <li class="{{request()->is('*admins/create') ? 'active' : ''}}"><a href="{{route('dashboard.admins.create')}}">Add Admin</a></li>
                                @endif
                        </ul>
                    </li>
                    @endif
                    @if(canDo($permissions,'read albums'))
                    <li>
                        <a href="#" class="{{request()->is('*albums*') ? 'active' : ''}}"><i class="icon-stack2"></i> <span>albums</span></a>
                        <ul>
                            <li class="{{request()->is('*albums*') ? 'active' : ''}}"><a href="{{route('dashboard.albums.index')}}">All albums</a></li>
                        </ul>
                    </li>
                        @endif
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
<!-- /main sidebar -->
