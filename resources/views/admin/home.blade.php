@extends('admin.layout.master')
@push('title','Home')

@section('content')
    <div class="row">
        <div class="col-lg-4">

            <!-- Members online -->
            <div class="panel bg-teal-400">
                <div class="panel-body">
                    <div class="heading-elements">
                        <span class="heading-text badge bg-teal-800"><span class="icon-user"></span></span>
                    </div>

                    <h3 class="no-margin">{{$data['users']}}</h3>
                    Users
                </div>
            </div>
            <!-- /members online -->

        </div>
        <div class="col-lg-4">

            <!-- Members online -->
            <div class="panel bg-teal-400">
                <div class="panel-body">
                    <div class="heading-elements">
                        <span class="heading-text badge bg-teal-800"><span class="icon-users"></span></span>
                    </div>

                    <h3 class="no-margin">{{$data['admins']}}</h3>
                    Admins
                </div>
            </div>
            <!-- /members online -->

        </div>
        <div class="col-lg-4">

            <!-- Members online -->
            <div class="panel bg-teal-400">
                <div class="panel-body">
                    <div class="heading-elements">
                        <span class="heading-text badge bg-teal-800"><span class="icon-gallery"></span></span>
                    </div>

                    <h3 class="no-margin">{{$data['albums']}}</h3>
                    Albums
                </div>
            </div>
            <!-- /members online -->

        </div>
    </div>
@endsection
