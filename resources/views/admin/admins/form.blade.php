<fieldset class="content-group">
    <div class="form-group">
        <label class="control-label col-lg-2">Email :</label>
        <div class="col-lg-10">
            {!! Form::email('email',null, ['class' => 'form-control','placeholder'=>'Example@example.com']) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2">Password</label>
        <div class="col-lg-10">
            <input type="password" name="password" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-2">Password Confirmation</label>
        <div class="col-lg-10">
            <input type="password" name="password_confirmation" class="form-control">
        </div>
    </div>
    <br>
</fieldset>



<fieldset class="content-group">
    <h1>Permissions</h1>
    @foreach(\App\Permission::with('rules')->get() as $permission)
        <div class="form-group">
            <label class="control-label col-lg-2">{{$permission->name}}</label>
            <div class="col-lg-10">
                {!! Form::select('permission[]', $permission->rules->mapWithKeys(function ($q){
                    return [$q->id => $q->name];
                })
     , isset($admin) ? myPerm($admin->permissions(),$permission->name) : null , ['class' => 'form-control bootstrap-select','multiple']) !!}
            </div>
        </div>
    @endforeach
</fieldset>


<div class="form-group">
    <div class="col-lg-4 col-lg-offset-4">
        <input type="submit" value="Save" class="form-control btn btn-success">
    </div>
</div>
