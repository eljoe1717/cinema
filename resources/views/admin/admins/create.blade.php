@extends('admin.layout.master')
@push('title','Admins | Create')
@section('content')
<div class="panel">
    <div class="panel-body">
        {!! Form::open(['route' => 'dashboard.admins.store', 'method' => 'post']) !!}
        @csrf
        @include('admin.admins.form')
        {!! Form::close() !!}
    </div>
</div>
@endsection
