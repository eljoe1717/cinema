@extends('admin.layout.master')
@push('title','Admins')

@section('content')
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Email</th>
                <th class="text-center" style="width: 30px;"><i class="icon-menu-open2"></i></th>
            </tr>
            </thead>
            <tbody>
            @foreach($admins as $admin)
                <tr>
                    <td>{{$admin->email}}</td>
                    <td class="text-center">
                        <ul class="icons-list">
                            @if(canDo($permissions,'update admins'))
                                <li><a href="{{route('dashboard.admins.edit',$admin->id)}}" data-popup="tooltip" title="Edit"><i class="icon-pencil7"></i></a></li>
                            @endif
                            @if(canDo($permissions,'read admins'))
                                <li><a href="{{route('dashboard.admins.show',$admin->id)}}" class="delete" data-del="{{$admin->id}}" data-popup="tooltip" title="Remove"><i class="icon-trash"></i></a></li>
                            @endif
                        </ul>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $admins->links() !!}
    </div>

@endsection
@push('header')
@endpush
