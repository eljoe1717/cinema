@extends('admin.layout.master')
@push('title','Admins | Update')
@section('content')
    <div class="panel">
        <div class="panel-body">
            {!! Form::model($admin, ['route' => ['dashboard.admins.update', $admin->id], 'method' => 'PUT']) !!}
            @csrf
            @include('admin.admins.form')

            {!! Form::close() !!}
        </div>
    </div>
@endsection
