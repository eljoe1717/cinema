@extends('admin.layout.master')
@push('title','Admins')

@section('content')
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Name (ar)</th>
                <th>Name (en)</th>
                <th>Main Image</th>
                <th>Type</th>
                <th>Price</th>
                <th>Discount Percentage</th>
                <th>Owner</th>
                <th>Album Images</th>
                <th class="text-center" style="width: 30px;"><i class="icon-menu-open2"></i></th>
            </tr>
            </thead>
            <tbody>
            @foreach($albums as $album)
                <tr>
                    <td>{{$album->ar_name}}</td>
                    <td>{{$album->en_name}}</td>
                    <td>
                        <img src="{{$album->image}}" width="50" height="50">
                    </td>
                    <td>{{$album->type}}</td>
                    <td>{{$album->price}}</td>
                    <td>{{$album->discount_percentage}}</td>
                    <td>{{$album->user->name}}</td>
                    <td>
                        <a href="{{route('dashboard.albums.show',$album->id)}}"  data-toggle="modal" data-target="#exampleModal{{$album->id}}" class="btn btn-info">Show Album</a>
                    </td>
                    <td class="text-center">
                        <ul class="icons-list">
{{--                            <li><a href="{{route('dashboard.albums.edit',$album->id)}}" data-popup="tooltip" title="Edit"><i class="icon-pencil7"></i></a></li>--}}
                            <li><a href="javascript:" class="delete" data-del="{{$album->id}}" data-popup="tooltip" title="Remove"><i class="icon-trash"></i></a></li>
                        </ul>
                        <form action="{{route('dashboard.albums.destroy',$album->id)}}" class="delete{{$album->id}}" method="post">
                            @csrf
                            @method('DELETE')
                        </form>
                        <div class="modal fade" id="exampleModal{{$album->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Photos</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                            <ol class="carousel-indicators">
                                                @foreach($album->photos as $key=>$photo)
                                                <li data-target="#carouselExampleIndicators" data-slide-to="{{$key}}" class="{{$key == 0 ?'active' : ''}}"></li>
                                                    @endforeach
                                            </ol>
                                            <div class="carousel-inner">
                                                @foreach($album->photos as $key=>$photo)
                                                <div class="carousel-item {{$key == 0 ?'active' : ''}}">
                                                    <img class="d-block w-100" src="{{$photo->image}}" alt="First slide">
                                                </div>
                                                    @endforeach
                                            </div>
                                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $albums->links() !!}
    </div>
    <script>
        $('.delete').click(function () {
            alert('ss');
            $('.delete'+$(this).data('del')).submit();
        })
    </script>
@endsection
