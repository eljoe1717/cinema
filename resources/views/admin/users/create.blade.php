@extends('admin.layout.master')
@push('title','Users | Create')
@section('content')
<div class="panel">
    <div class="panel-body">
        {!! Form::open(['route' => 'dashboard.users.store', 'method' => 'post']) !!}
        @csrf
        @include('admin.users.form')
        {!! Form::close() !!}
    </div>
</div>
@endsection
