@extends('admin.layout.master')
@push('title','Admins | Update')
@section('content')
    <div class="panel">
        <div class="panel-body">
            {!! Form::model($user, ['route' => ['dashboard.users.update', $user->id], 'method' => 'PUT']) !!}
            @csrf
            @include('admin.users.form')

            {!! Form::close() !!}
        </div>
    </div>
@endsection
