@extends('admin.layout.master')
@push('title','Users')

@section('content')
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th class="text-center" style="width: 30px;"><i class="icon-menu-open2"></i></th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td class="text-center">
                    <ul class="icons-list">
                        <li><a href="{{route('dashboard.users.edit',$user->id)}}" data-popup="tooltip" title="Edit"><i class="icon-pencil7"></i></a></li>
                        <li><a href="{{route('dashboard.users.show',$user->id)}}" data-popup="tooltip" title="Remove"><i class="icon-trash"></i></a></li>
                    </ul>
                </td>
            </tr>
                @endforeach
            </tbody>
        </table>
        {!! $users->links() !!}
    </div>

@endsection
