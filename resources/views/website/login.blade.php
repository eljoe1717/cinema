@extends('website.layout.master')
@push('title','Login')
@section('content')
    <section class="contact-us bg-light">
        <div class="container">
            <h3 class="text-center">Login To Join Us</h3>

            <div class="row justify-content-center">
                <div class="col-md-7 col-sm-10">
                    <div class="contact-form">
                        <form action="{{route('login')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="inputEmail">Your Email Addrss</label>
                                <input type="email" name="email" value="{{old('email')}}" id="inputEmail" class="form-control {{isset($errors) && $errors->has('email') ? 'is-invalid' : ''}}"
                                       placeholder="Write Your Email" required>
                                @if(isset($errors) && $errors->has('email'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('email')}}
                                    </div>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="inputPassword">Your Password </label>
                                <input type="password" name="password" id="inputPassword" class="form-control {{isset($errors) && $errors->has('password') ? 'is-invalid' : ''}}" placeholder=" Write Your password" required>
                                @if(isset($errors) && $errors->has('password'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('password')}}
                                    </div>
                                @endif

                            </div>


                            <div class="text-center p-2">
                                <button type="submit" class="btn btn-gradiant">
                                    login
                                </button>
                            </div>

                            <div >
                                <b> <span>Don't Have An Account ?</span> <a href="{{url('register')}}" class="main-color ">Sign Up</a></b>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
