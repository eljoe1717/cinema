<!DOCTYPE html>
<html>

<head>
    <title> Smart Movies | @stack('title') </title>
    @include('website.layout.styles')
    @stack('styles')
</head>

<body>
<div class="body_wrapper">
@include('website.layout.nav')
    @yield('content')
@include('website.layout.footer')
    <span class="scroll-top">
        <a href="#">
            <i class="fas fa-chevron-up"></i>
        </a>
    </span>
    @include('website.layout.scripts')
    @stack('scripts')
</div>
</body>

</html>
