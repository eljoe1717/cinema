<meta charset="UTF-8" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="author" content="RoQaY">
<meta name="robots" content="index, follow">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content=" Smart Movies website">
<meta name="keywords" content=" Smart Movies ">
<meta name="csrf-token" content="{{csrf_token()}}">
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
{{---------------------------------------------------------------------------------------------------------------}}
<link rel="stylesheet" type="text/css" media="screen" href="{{asset('website/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" media="screen" href="{{asset('website/css/fontawesome.min.css')}}">
<link rel="stylesheet" type="text/css" media="screen" href="{{asset('website/css/animate.css')}}">
<link rel="stylesheet" type="text/css" media="screen" href="{{asset('website/css/all.min.css')}}">
<link rel="stylesheet" type="text/css" media="screen" href="{{asset('website/css/style.css')}}">
<link rel="stylesheet" type="text/css" media="screen" href="{{asset('website/css/responsive.css')}}">
