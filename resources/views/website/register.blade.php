@extends('website.layout.master')
@push('title','Register')

@section('content')
    <section class="contact-us bg-light">
        <div class="container">
            <h3 class="text-center">Sign Up To Join Us</h3>

            <div class="row justify-content-center">
                <div class="col-md-7 col-sm-10">
                    <div class="contact-form">
                        <form action="{{route('register')}}" method="post">
                            @csrf
                            <div class="form-group ">
                                <label for="inputName">Write Your Name</label>
                                <input type="text" name="name" id="inputName" value="{{old('name')}}" class="form-control {{isset($errors) && $errors->has('name') ? 'is-invalid' : ''}}"
                                       placeholder="Write Your Name" required>
                                @if(isset($errors) && $errors->has('name'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('name')}}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="inputEmail">Your Email Addrss</label>
                                <input type="email" name="email" id="inputEmail" value="{{old('email')}}" class="form-control {{isset($errors) && $errors->has('email') ? 'is-invalid' : ''}}"
                                       placeholder="Write Your Email" required>
                                @if(isset($errors) && $errors->has('email'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('email')}}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="inputPassword">Enter Password </label>
                                <input type="password" name="password" id="inputPassword" class="form-control {{isset($errors) && $errors->has('password') ? 'is-invalid' : ''}}" placeholder=" Write Your password" required>
                                @if(isset($errors) && $errors->has('password'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('password')}}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputConfirmPassword">Confirm Password </label>
                                <input type="password" name="password_confirmation" id="inputConfirmPassword"
                                       class="form-control {{isset($errors) && $errors->has('password_confirmation') ? 'is-invalid' : ''}}" placeholder="  Confirm Your password" required>
                                @if(isset($errors) && $errors->has('password_confirmation'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('password_confirmation')}}
                                    </div>
                                @endif
                            </div>

                            <div class="text-center p-2">
                                <button type="submit" class="btn btn-gradiant">
                                    Sign Up
                                </button>
                            </div>

                            <div >
                                <b> <span>Have An Account ?</span> <a href="{{url('login')}}" class="main-color ">Login</a></b>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
