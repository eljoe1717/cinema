@extends('website.layout.master')
@push('title','Home')

@section('content')
    <div id="app"></div>
@endsection
@push('scripts')
    <script src="{{asset('js/app.js')}}"></script>
@endpush
