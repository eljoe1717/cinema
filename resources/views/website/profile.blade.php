@extends('website.layout.master')
@push('title','Profile')
@section('content')
    <div id="profile"></div>
@endsection
@push('scripts')
    <script src="{{asset('js/profile.js')}}"></script>
@endpush
