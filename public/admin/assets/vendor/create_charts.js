(function ($) {
      var charts = {
          init : function () {
              // charts.createCompletedJobsChart();
              this.ajaxGetPostMonthlyData();
              
          },

          ajaxGetPostMonthlyData :function(){
            var urlPath ='http://'+ window.location.hostname + 'get_post_chart_data';
            var request =$.ajax({
                method: 'GET',
                url:urlPath
            });
            request.done(function (response) {
               charts.createCompletedJobsChart(response);
            });
          },

          createCompletedJobsChart : function () {

              var ctx =document.getElementById("myChart");
              var myLineChart = new Chart(ctx, {
                  type : 'line',
                  data :{
                      labels :response.labels,
                      datasets :[{
                          label : 'sessions',
                          lineTension : 0.3,
                          backgroundColor:"rgba(2,117,216,0,2)",
                          borderColor :"rgba(2,117.216,1)",
                          pointRadius :5,
                          pointBackgroundColor:"rgba(2,117.216,1)",
                          pointRadiusColor:"rgba(255,255,255,0,8)",
                          pointHoverRadius :5,
                          pointHoverBackgroundColor: "rgba(2,117.216,1)",
                          pointHitRadius: 20,
                          pointBorderWidth: 2,
                          data:response.data

                      }]
                  }
                  
              });

          }
      }
});